#! /bin/bash
#
# tools.sh
#
# Usage: ./dc-help.sh [action]
#
# Script to help in set up and use of the environment:
# * Setup the image from the docker file
# * Run the container from the image
#
# DEPRECATED Prefer the dc-help.sh file


BASE_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"
CONTAINER_NAME="websocket"
IMAGE_NAME="${CONTAINER_NAME}_image"
EXT_PORT=80
INT_PORT=80

function build_image {
	docker build -t ${IMAGE_NAME} ${BASE_PATH}/docker
}

function run_container {
    docker run -ti --name=$CONTAINER_NAME -v ${BASE_PATH}/src:/var/www \
            -p ${EXT_PORT}:${INT_PORT} ${IMAGE_NAME} "$@"
}

action=$1
shift
$action "$@"
