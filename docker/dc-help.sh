#! /bin/bash
#
# dc-help.sh
#
# Usage: ./dc-help.sh [action] [param]
#
# Script to help in container management (create / start / stop / delete / etc.)
# image(s).
#
# Available parameters:
#   - generate_yml
#   - create [force]
#   - delete
#   - start
#   - stop
#   - status
#   - connect
#
# NB: except for create and status, commands are more memo than really useful stuff...

set -o pipefail

PROJECT_NAME="websocket"
CONTAINER_NAME="${PROJECT_NAME//-/_}"
CONTAINER_NETWORK_NAME="${CONTAINER_NAME}_net"
BASE_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )/../" && pwd )"

function find_port {
    used_ports=$(netstat -ulante | awk '{ print $4 }' | grep -Ev '[a-zA-Z()]' | rev | cut -d':' -f1 | rev | sort | uniq)
    port=$1
    threshold=$2
    rslt=0
    while [ $rslt -ne 1 ]
    do
        ((port++))
        if [ $port -eq $threshold ]
        then
            echo "Maximum iteration reached - unable to find a valid port"
            exit 1
        fi
        # lsof return 1 if the given port is used
        # XXX This is a project dependency
        # TODO needs sudo access...
        #lsof -Pi :$port -sTCP:LISTEN &> /dev/null
        #used="$(echo "$used_ports")"
        #rslt=$?
        if [[ $used_ports != *"$port"* ]]
        then
            rslt=1
        fi
    done
    echo $port
}

function find_ip_base {
    chosen_ip=0
    threshold=250
    rslt=0
    USED_SUBNETS=$(docker network inspect $(docker network ls -q) --format '{{ .IPAM.Config }}')
    while [ $rslt -ne 1 ]
    do
        ((chosen_ip++))
        if [ $chosen_ip -eq $threshold ]
        then
            echo "Maximum iteration reached - unable to find a valid IP"
            exit 1
        fi
        subnet_ip=10.$chosen_ip.0.0
        # grep returns 1 if pattern not found
        echo "$USED_SUBNETS" | grep "$subnet_ip" &> /dev/null
        rslt=$?
    done
    echo 10.$chosen_ip.0
}

function generate_yml {
    echo "Find available port..."
    port="$(find_port 3000 3999)"

    echo "Find an available network..."
    ip_base=$(find_ip_base)
    subnet_ip="${ip_base}.0"
    gateway_ip="${ip_base}.1"

    # TODO VM's NginX will need the port and ip of the container for site conf.
    # TODO Use SSL (setup and conf)

    echo "Generating /tmp/${PROJECT_NAME}-compose.yml"
    cat > /tmp/${PROJECT_NAME}-compose.yml << EOF
version: '2.2'

services:
    web:
        build: ${BASE_PATH}/docker/
        container_name: ${CONTAINER_NAME}
        privileged: true
        tty: true
        networks:
          - net
        ports:
          - "$port:80"
        volumes:
          - ${BASE_PATH}/src:/var/www

networks:
    net:
        driver: bridge
        driver_opts:
            com.docker.network.enable_ipv4: "true"
        ipam:
            driver: default
            config:
              - subnet: $subnet_ip/16
                gateway: $gateway_ip
EOF
}

function create {
    opts=""
    if [[ $1 == *'force'* ]]
    then
        opts="--force-recreate"
    fi
    generate_yml
    docker-compose -p "${CONTAINER_NAME}" -f /tmp/${PROJECT_NAME}-compose.yml up --no-start $opts
}

function start {
    echo "Starting container..."
    docker start ${CONTAINER_NAME}
}

function stop {
    echo "Stopping container..."
    docker stop ${CONTAINER_NAME}
}

function delete {
    echo "Delete container..."
    docker rm ${CONTAINER_NAME}
    echo "Delete network..."
    docker network rm ${CONTAINER_NETWORK_NAME}
}

function status {
    fmt="{{.Names}}~{{.Status}}~{{.Ports}}"
    cttstat="$(docker ps -a --format "${fmt}" | grep "${CONTAINER_NAME}")"
    ret="$?"
    if [ $ret -gt 0 ]
    then
        echo "Container ${CONTAINER_NAME} not found."
        exit 1
    fi

    if [[ $cttstat == *"Exited"* ]]
    then
        echo "Container ${CONTAINER_NAME} is stopped."
    else
        cport=$(echo "$cttstat" | cut -d'~' -f3 | cut -d':' -f2)
        fmt="Image: {{.Image}}+++Created since: {{.CreatedAt}}+++Status: {{.Status}}"
        cstatus="$(docker ps -a --format "$fmt" | grep ${CONTAINER_NAME})"
        cip="$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${CONTAINER_NAME})"
        echo "Container ${CONTAINER_NAME} is running.
$cstatus
IP Address: $cip
Port Mapping: $cport" | sed 's/+++/\n/g'
    fi
}

function ipconf {
    fmt="{{.Names}}~{{.Status}}~{{.Ports}}"
    cttstat="$(docker ps -a --format "${fmt}" | grep "${CONTAINER_NAME}")"
    ret="$?"
    if [ $ret -gt 0 ]
    then
        echo "Container ${CONTAINER_NAME} not found."
        exit 1
    fi

    if [[ $cttstat == *"Exited"* ]]
    then
        echo "Container ${CONTAINER_NAME} is stopped."
    else
        cport=$(echo "$cttstat" | cut -d'~' -f3 | cut -d':' -f2)
        cip="$(docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${CONTAINER_NAME})"
        echo "$cip:${cport%-*}"
    fi
}

function connect {
    user=$1
    if [ "$user" == "" ]
    then
        user=centos
    fi
    docker exec -ti ${CONTAINER_NAME} su $user
}

action=$1
shift
$action "$@"
