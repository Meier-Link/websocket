/**
 * views.js
 *
 * List of the JS views of the application.
 */

views = {
    'left-side': {
        el: '#left-side',
        methods: {
            showLeft: function(ev) {
                document.querySelectorAll('.content').forEach(el => {
                    el.style.display = 'none';
                });
                document.getElementById('who-needs-help').style.display = 'block';
            }
        }
    },
    'right-side': {
        el: '#right-side',
        methods: {
            showRight: function(ev) {
                document.querySelectorAll('.content').forEach(el => {
                    el.style.display = 'none';
                });
                document.getElementById('ask-for-help').style.display = 'block';
            }
        }
    },
    'main': {
        el: '#main',
        data: {
            message: 'World'
        }
    },
    'who-needs-help': {
        el: '#who-needs-help',
        data: {
        }, methods: {
            close: function(ev) {
                document.querySelectorAll('.content').forEach(el => {
                    el.style.display = 'none';
                });
                document.getElementById('main').style.display = 'block';
            }
        }
    },
    'ask-for-help': {
        el: '#ask-for-help',
        data: {
        }, methods: {
            close: function(ev) {
                document.querySelectorAll('.content').forEach(el => {
                    el.style.display = 'none';
                });
                document.getElementById('main').style.display = 'block';
            }
        }
    }
};

maps = {
    'main': {
        tag: 'main-map',
        coords: [4.061, -52.915],
        zoom: 8,
        token: 'pk.eyJ1IjoibWVpZXItbGluayIsImEiOiJjanVlOHVnYjUwMHl0NGFxanJ1c2x5N290In0.ynvjWhLPGMqi1SDhxMAWVg',
        maxZoom: 18,
        minZoom: 7,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets',
        borders: {
            lng: { min:-51.50 , max: -54.40},
            lat: { min: 2.25, max: 5.80 }
        }
    }
};

window.onload = function() {
    /* Load views */
    var left_view = new Vue(views['left-side']);
    var rigth_view = new Vue(views['right-side']);
    var main_view = new Vue(views['main']);
    var who_needs_help_view = new Vue(views['who-needs-help']);
    var ask_for_help = new Vue(views['ask-for-help']);
    /* Load map */
    var main_map = L.map(maps['main'].tag).setView(maps['main'].coords, maps['main'].zoom);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' + maps['main'].token, {
        maxZoom: maps['main'].maxZoom,
        attribution: maps['main'].attribution,
        id: maps['main'].id,
        minZoom: maps['main'].minZoom
    }).addTo(main_map);
    main_map.on('drag', (e) => {
        console.log(e);
        /* TODO not perfect at all - bug when on a corner... */
        var latLng = main_map.getCenter()
        if (latLng.lat < maps['main'].borders.lat.min) {
            main_map.panTo(new L.LatLng(maps['main'].borders.lat.min, latLng.lng))
        }
        if (latLng.lat > maps['main'].borders.lat.max) {
            main_map.panTo(new L.LatLng(maps['main'].borders.lat.max, latLng.lng))
        }
        if (latLng.lng > maps['main'].borders.lng.min) {
            main_map.panTo(new L.LatLng(latLng.lat, maps['main'].borders.lng.min))
        }
        if (latLng.lng < maps['main'].borders.lng.max) {
            main_map.panTo(new L.LatLng(latLng.lat, maps['main'].borders.lng.max))
        }
    });
    console.log(main_map);
};
