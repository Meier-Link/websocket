const WebSocket = require('ws');  // XXX Only to run in node cli, next a npm install ws

const socket = new WebSocket('ws://localhost:8080');  // XXX Same port as given in the sample server.

socket.addEventListener('open', (ev) => {
    // Triggered when the connection is established
    // NB: it seems that "new WebSocket(...) above already established the connection oO
    socket.send('Hello, server!');
});

socket.addEventListener('message', (ev) => {
    // Triggered whenever the server send a message
    console.log('Message from server:', ev.data);
});

// Send a message to the server
socket.send('I want to talk to you, server...');
