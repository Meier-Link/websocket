# Notes relative to asynchronous calls, event loops, etc.

## AsyncIO event loop

asyncio provides an event loop to Python which makes possible to say "when 
happen, then do B".

Simply put, the event loop is reponsible to find subscribers to events and run
them when the event occurs.

**Idea:** This seems close to the concept of observers from where reactive
programming go deeper. So maybe we may add operators concept to the simple
event loop?

async-tests/asyncio_future.py demonstrates how to put both coroutines and plain
functions together in a event loop. We may imagine somehting based on this
observation to call functions simultaneously in an event loop in the
observable.next|error|complete methodes

asyncio also provides a convenient function to check if a function is a
coroutine or not. So we may check if the subscriber is a coroutine or not and
adapt the way we handle it. (i.e. using or not a thread pool executor)

**NB:** What remains to study is how to manage observers errors. 