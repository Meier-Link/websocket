#! /usr/bin/env python3

"""An observable emits item whenever it has something which happens on the stream.

    An observable may emits 0 or more items with next, 0 or 1 notification with error
    (if it encounters an error), and 0 or 1 notification with complete (which happens
    if the observable encounters the end of its emission).
    It implies that:
    * An observable may do 'just' nothing;
    * An observable may terminate (complete) immediately;
    * An observable can emit indefinitely.
"""

import collections
from .observer import Observer


class RxException(BaseException):
    pass


class Observable:
    """The simplest Observable implementation ever."""
    # TODO: implement a staticmethod able to receives a generator and create an
    # observable from.
    def __init__(self, hot=True):
        """Observers must works like Observer instances.

            Hot observable may emit items as soon as it is created.
            Cold observable emit nothing until someone subscribes to. This means
            whatever emitted before is lost.

            @param hot: whether the observable may starts now (hot) or wait to
                have subscribers (cold).
        """
        self.__observers = []
        self.__alive = True
        self._hot = hot

    def subscribe(self, observer):
        """Add the given observer as an observer of the Observable.

        If the observer is a function, Observable().subscribe can be used as a
        decorator.
        NB: in that last case, the function is transfmored in an Observer instance,
        which has the __call__ method.

        @param observer: Callable or Observer instance.
        @return the obeserver.
        """

        if isinstance(observer, collections.Callable):
            observer = Observer(observer)
        self.__observers.append(observer)
        return observer

    def next(self, item=None):
        """Whenever the emitter emits, the observable's next is called."""
        # TODO maybe launch subscribers simultaneously in the async way
        # TODO why not __anext__ and __aiter__ ? It looks like a generator
        if not self.__alive:
            raise RxException("This observable cannot emit items since it's dead.")

        if self._hot:
            for observer in self.__observers:
                if observer.accept_item:
                    observer.on_next(item)
                else:
                    observer.on_next()

    def error(self, err=None):
        """Provides the error to the observers."""
        # TODO maybe launch subscribers simultaneously in the async way
        if not self.__alive:
            raise RxException("This observable is already dead.")

        if self._hot:
            for observer in self.__observers:
                observer.on_error(err)

        # I'll no more emit anything.
        self.__observers = []
        self.__alive = False

    def complete(self, item=None):
        """Provides the itemor to the observers."""
        # TODO maybe launch subscribers simultaneously in the async way
        # TODO the pythonic way consist in using Stop(Async)Iteration here.
        if not self.__alive:
            raise RxException("This observable is already dead.")

        if self._hot:
            for observer in self.__observers:
                observer.on_complete(item)

        # I'll no more emit anything.
        self.__observers = []
        self.__alive = False


class Connectable(Observable):
    """Observable which becomes hot only when its connect method is called.

        This kind of observable emits nothing until its connect method is not called.
        So the hot/cold behavior is disabled.
    """
    def __init__(self):
        return super().__init__(False)

    def connect(self):
        """Start emitting items even if no observers."""
        self._hot = True
