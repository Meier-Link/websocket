#! /usr/bin/env python3

import collections
from inspect import signature


class Observer:
    """Base class for any observer which follows an observable.

        The usual way consist in using @observable.subscribe decorator on a function
        which must perform actions when the observable emits with next().
        In fact, the observable.subscribe converts the decorated function in an
        instance of Observer.
        But to support error and complete, its better to create a class which inherits
        from this Observer abstract class.
    """
    def __init__(self, func=None):
        self.__func = func
        self.__arg_len = 1
        if func is not None:
            sign = signature(func)
            if len(list(sign.parameters)) > 1:
                raise TypeError('Observer cannot accepts more than 1 argument.')
            self.__arg_len = len(list(sign.parameters))

    @property
    def accept_item(self):
        """Tells if the observer accepts or not items."""
        return bool(self.__arg_len)

    def on_next(self, item=None):
        """Called whenever somehting happened in the observable side.

        @param item: the item the observer observes.
        """
        if isinstance(self.__func, collections.Callable):
            # Not sure that's reactive compliant...
            # try:
            return self.__func(item)
            # except StopIteration:
            #   self.on_complete()
        return item

    def on_error(self, err: BaseException=None):
        """Called whenever the observable encounters an error.

        Here the observer determines what to do with the exception errord by the
        observable.
        By default, the exception is errord. This is up to observer implementation
        to choose what's best to do here.

        @param err: The error errord by the observable.
        """
        raise err

    def on_complete(self, item=None):
        """Called after the observable has emitted its last item."""
        # TODO the pythonic way consist in using Stop(Async)Iteration here.
        pass

    def __call__(self, item=None):
        """An observer must behave as a callable for homogeneity.

        @param item: the item the observer observes.
        """
        return self.on_next(item)
