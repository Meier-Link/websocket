#! /usr/bin/env python3

"""Naive implementation of a subject.

    A Subject is both an observer and an observable which receives items emitted
    by an observable, and an observable which emits the items.
    It may apply operations on received items, add its own emitted items, etc.
    The simplest subject simply emits received items.
"""

from observable import Observable
from observer import Observer


class Subject(Observable, Observer):
    """A subject is both an observable which emits items and an observer which
        itself receives items."""
    pass
