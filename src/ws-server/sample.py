#!/bin/env python3

from SimpleWebSocketServer import SimpleWebSocketServer, WebSocket

class SimpleEcho(WebSocket):
    def handleMessage(self):
        print("Received: " + self.data)
        self.sendMessage("Received: " + self.data)

    def handleConnected(self):
        print(self.address, 'connected')
        self.sendMessage("Yup from the server o/")

    def handleClose(self):
        print(self.address, 'closed')

if __name__ == '__main__':
    server = SimpleWebSocketServer('', 8080, SimpleEcho)
    server.serveforever()
