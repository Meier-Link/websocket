#! /usr/bin/env python3

import asyncio
import time
from concurrent import futures


@asyncio.coroutine
def countdown(name, num):
    while num > 0:
        print('{}: {}s before death...'.format(name, num))
        yield from asyncio.sleep(1)
        num -= 1
    print('{} is dead.'.format(name))


def heroes_success(name, num):
    while num > 0:
        print('{} about to win in {}s...'.format(name, num))
        time.sleep(1)
        num -= 1
    print('{} won'.format(name))

async def heroes_coro(heroes):
    workers = min(20, len(heroes))
    with futures.ThreadPoolExecutor(workers) as executor:
        calls = [
            executor.submit(heroes_success, hero, num) for hero, num in heroes.items()
        ]
    return await calls

heroes = {
    'Den': 8,
    'Meier': 5,
    'Lucian': 4,
    'Freya': 4
}



if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    tasks = [
        asyncio.ensure_future(countdown("Fafnir", 5)),
        asyncio.ensure_future(countdown("Nidhogg", 3)),
        asyncio.ensure_future(countdown("Xenuvia", 8)),
        #asyncio.ensure_future(heroes_coro(heroes))
    ]
    tasks += [loop.run_in_executor(futures.ThreadPoolExecutor(1), heroes_success, name, num)
              for name, num in heroes.items()]
    loop.run_until_complete(asyncio.wait(tasks))
    loop.close()
