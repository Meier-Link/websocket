/**
 * Map Model
 *
 * This file define a models for map if we try to create a mini MMORPG game to
 * test websockets.
 * NB: we want to make something very light right now.
 *
 * We imagine maps are not scrollable (they can become scrollable later, but...
 * Hey, we said 'very light'!
 * TODO add more define here
 */

var MapModel = {
    'name': '',  // Name of the map to retreive it.
    'background': {
        'image': '',  // path to an image which is used as a background
        'color': ''  // a simple color used as background
        // Both can be used together (for example to fill in the transparency
        // of a PNG or GIF.
    }
    'layers': {
        // Maps will be built based on layer from bottom to top.
        // Layers contains objects (interactive or not) to put on the map
        'back': [
            // Back layers are rendered before living objects (PNJs, PJs, etc.)
            {
                // Define an object on the layer. It must be as simple as a
                // color spot or more complex like an image with animat... no.
                // No animated images for now ^^'
                'image': '',
                'color': // As for background, can be used to fill transparency
                'coordinates': [],  // x and y top left coordinates of the object
                'size': [],  // widget and height of the object
                'hitbox': true,  // (to tells animated objects cannot go through it)
                'onHit': () => {},  // Function triggered when enters the zone
                'onClick': () => {}, // Triggered if the player click on this object
            }
        ]
        'front': [
            // Front layers are rendered after living objects (PNJs, PJs, etc.)
            {
                // Define an object on the layer. It must be as simple as a
                // color spot or more complex like an image with animat... no.
                // No animated images for now ^^'
                'image': '',
                'color': // As for background, can be used to fill transparency
                'coordinates': [],  // x and y top left coordinates of the object
                'size': [],  // widget and height of the object
                'hitbox': true,  // (to tells animated objects cannot go through it)
                'onHit': () => {},  // Function triggered when enters the zone
                'onClick': () => {}, // Triggered if the player click on this object
            }
        ]
    }
};
